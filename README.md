# Insurance - Quoter

> This project is an online insurance quoter developed with React and TailwindCSS.

## Instructions

Follow these steps to run the project locally:

1. Clone the repository to your local machine:
   bash

```bash
git clone https://gitlab.com/LucianoJavierCostaPeralta/insurance-quoter.git
```

2. Install the dependencies using the following command that works with both Yarn and npm:

```bash
cd insurance-quoter

yarn
or
npm install
```

3. Run the development server:

```bash
yarn dev
or
npm run dev
```

4. Open the project in your browser by entering the following URL:

http://localhost:5173/

### Tech Stack

- [Vite](https://vitejs.dev/): Build and development system for web applications.
- [React](https://reactjs.org/): JavaScript library for creating interactive and reusable user interfaces.
- [Tailwind CSS](https://tailwindcss.com/): CSS design framework for creating customizable styles.
- [Context](https://reactjs.org/docs/context.html): Feature of React for sharing global data between components.

[![My Application](https://img.shields.io/badge/App-My%20Application-9cf)](https://insurance-quoter.vercel.app/)
