import { createContext, useState } from "react";
import {
  calculateAccordingToBrand,
  calculateAccordingToPlan,
  formatCurrency,
  getYearDifference,
} from "../helpers";

const QuoterContext = createContext();

const QuoterProvider = ({ children }) => {
  const [datos, setDatos] = useState({
    brand: "",
    year: "",
    plan: "",
  });

  const [error, setError] = useState("");

  const [result, setResult] = useState(0);

  const [loader, setLoader] = useState(false);

  const handleChangeDatos = (e) => {
    setDatos({
      ...datos,
      [e.target.name]: e.target.value,
    });
  };

  const calculateInsuranceQuoter = () => {
    let result = 2000;

    const dif = getYearDifference(datos.year);

    result -= (dif * 3 * result) / 100;

    result *= calculateAccordingToBrand(datos.brand);

    result *= calculateAccordingToPlan(datos.plan);

    result = formatCurrency(result);

    setLoader(true);

    setTimeout(() => {
      setResult(result);
      setLoader(false);
    }, 3000);
  };

  return (
    <QuoterContext.Provider
      value={{
        datos,
        handleChangeDatos,
        error,
        setError,
        calculateInsuranceQuoter,
        result,
        loader,
      }}
    >
      {children}
    </QuoterContext.Provider>
  );
};

export { QuoterProvider };

export default QuoterContext;
