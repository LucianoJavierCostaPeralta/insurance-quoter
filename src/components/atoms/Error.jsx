import React from "react";
import useQuoter from "../../hooks/useQuoter";
import { Fragment } from "react";

const Error = () => {
  const { error } = useQuoter();

  return (
    <Fragment>
      {error && (
        <div
          className="border border-red-400 bg-red-100 text-center py-3
            text-red-700 "
        >
          <p>{error}</p>
        </div>
      )}
    </Fragment>
  );
};

export default Error;
