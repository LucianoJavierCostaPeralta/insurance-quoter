import useQuoter from "../../hooks/useQuoter";

import { Spinner } from "../atoms/Spinner";
import FormComponent from "../molecules/FormComponent";
import Result from "../molecules/Result";

const InsuranceComponent = () => {
  const { loader } = useQuoter();

  return (
    <>
      <header className="my-10">
        <h1 className="text-white  text-center text-4xl font-black ">
          Car insurance quoter
        </h1>
      </header>

      <main
        className=" bg-white md:w-2/3 lg:w-2/4 mx-auto shadow rounded-lg
      p-10"
      >
        <FormComponent />

        {loader ? <Spinner /> : <Result />}
      </main>
    </>
  );
};

export default InsuranceComponent;
