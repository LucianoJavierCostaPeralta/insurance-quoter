import { Fragment } from "react";
import { BRANDS, PLANS, YEARS } from "../../constans";
import useQuoter from "../../hooks/useQuoter";
import Error from "../atoms/Error";

const FormComponent = () => {
  const {
    datos,
    handleChangeDatos,
    error,
    setError,
    calculateInsuranceQuoter,
  } = useQuoter();

  const element = (e) => handleChangeDatos(e);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (Object.values(datos).includes("")) {
      setError("All fields are required");
      return;
    }

    setError("");
    calculateInsuranceQuoter();
  };

  return (
    <>
      {error && <Error />}
      <form onSubmit={handleSubmit}>
        <div className="my-5">
          <label className="block mb-3 font-bold text-gray-400 uppercase">
            Brand
          </label>
          <select
            name="brand"
            className="w-full p-3 bg-white border border-gray-200"
            onChange={element}
            value={datos.brand}
          >
            <option value="">-- Select a brand --</option>
            {BRANDS?.map((item) => (
              <option key={item.id} value={item.id}>
                {item.name}
              </option>
            ))}
          </select>
        </div>

        <div className="my-5">
          <label className="block mb-3 font-bold text-gray-400 uppercase">
            Year
          </label>
          <select
            name="year"
            className="w-full p-3 bg-white border border-gray-200"
            onChange={element}
            value={datos.year}
          >
            <option value="">-- Select a year --</option>
            {YEARS?.map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
          </select>
        </div>

        <div className="my-5">
          <label className="block mb-3 font-bold text-gray-400 uppercase">
            Plan
          </label>

          <div className="flex gap-3 items-center">
            {PLANS?.map((item) => (
              <Fragment key={item.id}>
                <label>{item.name}</label>
                <input
                  type="radio"
                  name="plan"
                  value={item.id}
                  onChange={element}
                />
              </Fragment>
            ))}
          </div>
        </div>

        <input
          type="submit"
          className="w-full bg-indigo-500 hover:bg-indigo-600 p-3 uppercase
          transition-colors text-white cursor-pointer font-bold"
          value="Quoter"
        />
      </form>
    </>
  );
};

export default FormComponent;
