import { BRANDS, PLANS } from "../../constans";
import { getNameById } from "../../helpers";
import useQuoter from "../../hooks/useQuoter";
import { Fragment, useRef } from "react";

const Result = () => {
  const { result, datos } = useQuoter();

  const { brand, year, plan } = datos;

  const yearRef = useRef(year);

  const Summary = () => (
    <div className="flex  flex-col items-center  bg-gray-100  mt-5 py-5 shadow">
      <h2 className="text-gray-600 font-black text-3xl">Summary</h2>

      <div className="flex flex-col justify-start">
        <p className="mt-2 mb-0 ">
          <span className="font-semibold">Brand&nbsp;:&nbsp;&nbsp;</span>
          {nameBrand.name ?? null}
        </p>
        <p className="mt-2 mb-0">
          <span className="font-semibold">Plan&nbsp;:&nbsp;&nbsp;</span>
          {namePlan.name ?? null}
        </p>
        <p className="mt-2 mb-0">
          <span className="font-semibold">Year&nbsp;:&nbsp;&nbsp;</span>
          {yearRef.current ?? null}
        </p>
      </div>

      <p className="mt-5 text-2xl text-center">
        <span className="font-semibold">Total&nbsp;:&nbsp;&nbsp;</span>
        {result ?? null}
      </p>
    </div>
  );

  const nameBrand = getNameById(BRANDS, brand, result);
  const namePlan = getNameById(PLANS, plan, result);

  if (result === 0) {
    return null;
  }

  return <Fragment>{result === 0 ? <Fragment /> : <Summary />}</Fragment>;
};

export default Result;
