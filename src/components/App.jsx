import { QuoterProvider } from "../context/QuoterProvider";
import InsuranceComponent from "./organisms/InsuranceComponent";

function App() {
  return (
    <QuoterProvider>
      <InsuranceComponent />
    </QuoterProvider>
  );
}

export default App;
