import { useMemo } from "react";

export const getYearDifference = (year) => new Date().getFullYear() - year;

export const calculateAccordingToBrand = (brand) => {
  let item;

  switch (brand) {
    case "1":
      item = 1.3;
      break;
    case "2":
      item = 1.15;
      break;
    case "3":
      item = 1.05;
      break;

    default:
      break;
  }

  return item;
};

export const calculateAccordingToPlan = (plan) => (plan === "1" ? 1.2 : 1.5);

export const formatCurrency = (cant) =>
  cant.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });

export const getNameById = (array, id, res) =>
  useMemo(() => array.find((element) => element.id === Number(id)), [res]);
